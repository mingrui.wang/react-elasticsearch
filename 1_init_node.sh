elasticsearch \
  -Ecluster.name=my-movie-database \
  -Enode.name=movie-node-1 \
  -Epath.data=`pwd`/data \
  -Epath.logs=`pwd`/logs \
  -Ehttp.port=9200 \
  -Ehttp.cors.enabled=true \
  -Ehttp.cors.allow-origin=http://localhost:3000 \
  -Ehttp.cors.allow-methods="OPTIONS,HEAD,GET,POST,PUT,DELETE" \
  -Ehttp.cors.allow-headers="X-Requested-With,X-Auth-Token,Content-Type,Content-Length"
