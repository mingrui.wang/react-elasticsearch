import os
import requests
from copy import deepcopy


ELASTIC_PORT = 'http://localhost:9200'
# ALL_INDEXES = ['actors', 'genres', 'parts', 'taggings', 'movies']
ALL_INDEXES = ['movies', 'suggestion', 'suggestion_phrases', 'suggestion_words']


DATE_TYPE = {
    'type': 'object',
    'properties': {
        'year'     : {'type': 'integer'},
        'month'    : {'type': 'integer'},
        'day'      : {'type': 'integer'},
        'timestamp': {'type': 'integer'}
    }
}

AUTOCOMPLETE_SETTINGS = {
    'analysis': {
        'analyzer': {
            'keyword_analyzer': {
                'filter': [
                    'classic',
                    'lowercase',
                    'asciifolding',
                    'trim'
                ],
                'char_filter': [],
                'type': 'custom',
                'tokenizer': 'keyword'
            },
            'edge_ngram_analyzer': {
                'tokenizer': 'edge_ngram_tokenizer',
                'filter': ['classic', 'lowercase']
            },
            'edge_ngram_search_analyzer': {
                'tokenizer': 'lowercase',
                'filter': ['classic']
            }
        },
        'tokenizer': {
            'edge_ngram_tokenizer': {
                'type': 'edge_ngram',
                'min_gram': 2,
                'max_gram': 10,
                'token_chars': ['letter']
            }
        }
    }
}

AUTOCOMPLETE_FIELDS = {
    'keywordstring': {
        'type': 'text',
        'analyzer': 'keyword_analyzer'
    },
    'edgengram': {
        'type': 'text',
        'analyzer': 'edge_ngram_analyzer',
        'search_analyzer': 'edge_ngram_search_analyzer'
    },
    'completion': {
        'type': 'completion'
    }
}


def main():
    for index_name in ALL_INDEXES:
        requests.delete(os.path.join(ELASTIC_PORT, index_name))

    # Movies mapping
    movies_uri = os.path.join(ELASTIC_PORT, 'movies')
    movies_mapping = {
        'mappings': {'movies': {
            'properties': {
                'id'          : {'type': 'integer'},
                'external_id' : {'type': 'integer'},
                'title'       : {'type': 'text'   },
                'description' : {'type': 'text', 'analyzer': 'english'},
                'budget'      : {'type': 'integer'},
                'picture'     : {'type': 'keyword'},
                'release_date': DATE_TYPE,
                'actors'      : {'type': 'text'   },
                'genres'      : {'type': 'text', 'analyzer': 'english'}
            }
        }},
        'settings': {
            'number_of_shards': 5,
            'number_of_replicas': 0
        }
    }

    requests.put(movies_uri, json=movies_mapping)

    # Phrase complete mappings
    suggestion_settings = deepcopy(AUTOCOMPLETE_SETTINGS)
    suggestion_settings['number_of_shards'] = 3
    suggestion_settings['number_of_replicas'] = 0

    suggestion_uri = os.path.join(ELASTIC_PORT, 'suggestion_phrases')
    suggestion_mappings = {
        'settings': suggestion_settings,
        'mappings': {
            'suggestion_phrases': {
                'properties': {
                    'phrase': {
                        'type': 'text',
                        'analyzer': 'standard',
                        'fields': AUTOCOMPLETE_FIELDS
                    }
                }
            }
        }
    }

    requests.put(suggestion_uri, json=suggestion_mappings)

    # Word complete mappings
    suggestion_settings = deepcopy(AUTOCOMPLETE_SETTINGS)
    suggestion_settings['number_of_shards'] = 3
    suggestion_settings['number_of_replicas'] = 0

    suggestion_uri = os.path.join(ELASTIC_PORT, 'suggestion_words')
    suggestion_mappings = {
        'settings': suggestion_settings,
        'mappings': {
            'suggestion_words': {
                'properties': {
                    'word': {
                        'type': 'text',
                        'analyzer': 'standard',
                        'fields': AUTOCOMPLETE_FIELDS
                    }
                }
            }
        }
    }

    requests.put(suggestion_uri, json=suggestion_mappings)


if __name__ == '__main__':
    main()
