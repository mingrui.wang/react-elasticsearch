import os
import six
import tqdm
import datetime
import collections
from functools import reduce

import numpy as np
import pandas as pd

# We need someway to communitate with elastic, there are 2 ways, by sending
# http requests or through the elastic client interface
import json
import requests

# There is an open source movie database stored in sqlite3 format
import sqlite3


ELASTIC_PORT = 'http://localhost:9200'
ALL_INDEXES = ['actors', 'genres', 'parts', 'taggings', 'movies']
DATABASE_PATH = './db/movies.sqlite3'
JSON_HEADER = {'Content-type': 'application/json', 'Accept': 'text/plain'}


def encode(obj):
    if isinstance(obj, six.string_types):
        return obj.encode()
    elif isinstance(obj, collections.Mapping):
        return {k: encode(v) for k, v in obj.items()}
    elif isinstance(obj, collections.Sequence):
        return [encode(s) for s in obj]

    return obj


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as e:
        print(e)

    return None


def load_table(conn, table_name):
    cur = conn.cursor()

    # Get table column names
    cur.execute('PRAGMA table_info({});'.format(table_name))
    col_infos = cur.fetchall()
    col_names = [i[1] for i in col_infos]

    # Get table data
    cur.execute('SELECT * FROM {}'.format(table_name))
    table_data = cur.fetchall()

    # Format table data into df
    df = pd.DataFrame(table_data, columns=col_names)

    return df


def load_movie_id_to_actor_names():
    conn = create_connection(DATABASE_PATH)

    with conn:
        actors_df = load_table(conn, 'actors')
        parts_df  = load_table(conn, 'parts')

    pos_id   = np.argwhere(actors_df.columns == 'id')[0, 0]
    pos_name = np.argwhere(actors_df.columns == 'name')[0, 0]
    actor_id_to_name = {}
    for entry in actors_df.values:
        actor_id = entry[pos_id]
        actor_name = entry[pos_name]

        actor_id_to_name[actor_id] = actor_name

    pos_movie = np.argwhere(parts_df.columns == 'movie_id')[0, 0]
    pos_actor = np.argwhere(parts_df.columns == 'actor_id')[0, 0]
    movie_id_to_actor_names = {}
    for entry in parts_df.values:
        movie_id = entry[pos_movie]
        actor_id = entry[pos_actor]
        actor_name = actor_id_to_name[actor_id]

        if movie_id in movie_id_to_actor_names:
            movie_id_to_actor_names[movie_id] += [actor_name]
        else:
            movie_id_to_actor_names[movie_id] = [actor_name]

    return movie_id_to_actor_names


def load_movie_id_to_genres():
    conn = create_connection(DATABASE_PATH)

    with conn:
        genres_df = load_table(conn, 'genres')
        taggings_df = load_table(conn, 'taggings')

    pos_id   = np.argwhere(genres_df.columns == 'id')[0, 0]
    pos_name = np.argwhere(genres_df.columns == 'name')[0, 0]
    genre_id_to_name = {}
    for entry in genres_df.values:
        genre_id = entry[pos_id]
        genre_name = entry[pos_name]

        genre_id_to_name[genre_id] = genre_name

    pos_movie = np.argwhere(taggings_df.columns == 'movie_id')[0, 0]
    pos_genre = np.argwhere(taggings_df.columns == 'genre_id')[0, 0]
    movie_id_to_genres = {}
    for entry in taggings_df.values:
        movie_id = entry[pos_movie]
        genre_id = entry[pos_genre]
        genre_name = genre_id_to_name[genre_id]

        if movie_id in movie_id_to_genres:
            movie_id_to_genres[movie_id] += [genre_name]
        else:
            movie_id_to_genres[movie_id] = [genre_name]

    return movie_id_to_genres


def bulk_indexing(df, table_name, type_name=None, batch_size=64):
    if type_name is None:
        type_name = table_name

    uri = os.path.join(ELASTIC_PORT, '_bulk')

    df_values = df.values
    df_col_names = df.columns
    for ii in tqdm.tqdm(range(0, len(df), batch_size)):
        entries = df_values[ii:(ii+batch_size)]

        index_query = json.dumps({
            'index': {
                '_index': table_name,
                '_type': type_name
            }
        })
        payload = []
        for entry in entries:
            entry = {k:v for k,v in zip(df_col_names, entry)}

            payload += [index_query]
            payload += [json.dumps(entry)]

        res = requests.post(
            uri,
            headers=JSON_HEADER,
            data='\n'.join(payload) + '\n'
        )

    return


def main():
    movie_id_to_actor_names = load_movie_id_to_actor_names()
    movie_id_to_genres = load_movie_id_to_genres()

    conn = create_connection(DATABASE_PATH)
    with conn:
        movies_df = load_table(conn, 'movies')
        actors_df = load_table(conn, 'actors')
        genres_df = load_table(conn, 'genres')

    print('Making movies table')
    master_df = movies_df.copy()
    master_df['release_date'] = master_df['release_date'].apply(lambda x: None if pd.isnull(x) else datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f'))
    master_df['release_date'] = master_df['release_date'].apply(lambda x: {
        'year'   : x.year,
        'month'  : x.month,
        'day'    : x.day,
        'curtime': datetime.datetime.timestamp(x)
    } if not pd.isnull(x) else None)

    master_df['actors'] = master_df['id'].apply(lambda x: movie_id_to_actor_names[x] if x in movie_id_to_actor_names else [])
    master_df['genres'] = master_df['id'].apply(lambda x: movie_id_to_genres[x] if x in movie_id_to_genres else [])

    master_df = master_df.dropna()
    master_df.index = range(len(master_df))

    master_df_col_names = ['id', 'title', 'description', 'budget', 'release_date', 'actors', 'genres']
    master_df = master_df[master_df_col_names].copy()

    print('Uploading movies database')
    bulk_indexing(master_df, 'movies')

    print('Making autocomplete table')
    suggestion_terms = master_df['title'].copy().tolist() + \
        actors_df['name'].copy().tolist() + \
        genres_df['name'].copy().tolist()

    print('Uploading autocomplete phrases')
    suggestion_df = pd.DataFrame(suggestion_terms, columns=['phrase'])
    bulk_indexing(suggestion_df, 'suggestion_phrases')

    suggestion_words = [t.split() for t in suggestion_terms]
    suggestion_words = reduce(lambda a,b : a + b, suggestion_words)
    suggestion_words = list(set(suggestion_words))
    print('Uploading autocomplete words')
    suggestion_df = pd.DataFrame(suggestion_words, columns=['phrase'])
    bulk_indexing(suggestion_df, 'suggestion_words')


if __name__ == '__main__':
    main()
