import React, { Component } from 'react';

// Styles
import './css/App.css';
import './css/AppHeader.css'

// Objects
import AppHeader from './js/AppHeader';
import LeftBar from './js/LeftBar';
import ResultStats from './js/ResultStats';
import ResultEntry from './js/ResultEntry';

// Search methods
import { searchMovies } from './js/search';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time_taken: null,
      num_results: 0,
      results: []
    };
  }

  handleSearch(e, query) {
    e.preventDefault()
    searchMovies(query, 0, 10, (err, resp) => {
      this.setState({
        time_taken: resp.time_taken,
        num_results: resp.num_results,
        results: resp.results,
      })
    })
  }

  render() {
    let results = this.state.results.map((result, i) => {
      return <ResultEntry key={i} data={result} />
    });

    console.log(this.state)

    return (
      <div className="App">
        <AppHeader search={(e, query) => this.handleSearch(e, query)}/>
        <div className="Result-Container">
          <LeftBar />
          <div>
            <ResultStats time_taken={this.state.time_taken} num_results={this.state.num_results} />
            <ul className="Result-entry-container">
              {results}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
