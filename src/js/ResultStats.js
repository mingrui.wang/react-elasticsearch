import React, { Component } from 'react';


const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};


const roundToString = (x, n) => {
  const mult = 10 ** n;
  return (Math.round(x * mult) / mult).toString();
};


class ResultStats extends Component {
  render() {
    let message;
    if (this.props.time_taken != null) {
      message = 'About ' + numberWithCommas(this.props.num_results) + ' results (' + roundToString(this.props.time_taken, 5) + ' seconds)';
    };

    return (
      <div className="Result-Stats">
        {message}
      </div>
    );
  };
};

export default ResultStats;
