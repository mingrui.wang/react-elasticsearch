import React, { Component } from 'react';
import $ from 'jquery';

// Images
import shopee_logo from '../images/shopee.png';
import react_logo from '../images/logo.svg';

// Search methods
import { makeSuggestionsA, makeSuggestionsB } from './search';
const makeSuggestions = makeSuggestionsA


class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cursor_pos: -1,
      suggestions: [],
    };
  }

  unselectCursorPos(pos) {
    if (pos === -1) return;
    $('#Search-suggestion-' + pos.toString()).removeClass('selected');
  }

  selectCursorPos(pos) {
    if (pos === -1) return;
    $('#Search-suggestion-' + pos.toString()).addClass('selected');
  }

  handleCursorPos(shift) {
    this.unselectCursorPos(this.state.cursor_pos)

    let new_pos = this.state.cursor_pos + shift;

    if (new_pos >= this.state.suggestions.length) {new_pos = -1};
    if (new_pos < -1) {new_pos = this.state.suggestions.length - 1};

    this.setState({
        'cursor_pos': new_pos,
        'suggestions': this.state.suggestions
    })

    this.selectCursorPos(new_pos)
  }

  handleKeyUp(e) {
    let query = $('#Search-input').val();
    switch (e.which) {
      case 13:
        // Enter
        if (this.state.cursor_pos !== -1) {
          const selected_text = $('#Search-suggestion-' + this.state.cursor_pos.toString()).text()
          $('#Search-input').val(selected_text);
          query = selected_text;
        };

        this.props.search(e, query);

        this.setState({
          'cursor_pos': -1,
          'suggestions': []
        });

        break;

      case 38:
        // Up
        e.preventDefault();
        this.handleCursorPos(-1);
        break;

      case 40:
        // Down
        e.preventDefault();
        this.handleCursorPos(1);
        break;

      default:
        this.unselectCursorPos(this.state.cursor_pos)
        makeSuggestions(query, (err, resp) => {
          if (err) return;

          // Set state
          this.setState({
            'cursor_pos': -1,
            'suggestions': resp
          });
        });

        break;
    }
  }

  render() {
    let suggestions = this.state.suggestions.map((s, i) => {
      return (<li key={i} id={"Search-suggestion-" + i}>{s}</li>);
    });

    return (
      <header className="App-header">
        <img src={shopee_logo} className="App-logo App-logo1" alt="logo" />
        <img src={react_logo} className="App-logo App-logo2" alt="logo" />
        <div className="Search-bar-container">
          <form
            className="Search-bar shadow" id="Search-bar"
            autoComplete="off"
            onSubmit={e => this.props.search(e)}
            onKeyUp={e => this.handleKeyUp(e)}
          >
              <div className="Search-input-container">
                <input className="Search-input no-focus" id="Search-input" />
              </div>
              <button className="Search-button no-focus"><span className="glyphicon glyphicon-search"></span></button>
          </form>
          <ul className="Search-bar-dropdown shadow">
            {suggestions}
          </ul>
        </div>
      </header>
    )
  }
}

export default AppHeader;
