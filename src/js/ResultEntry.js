import React, { Component } from 'react';


class ResultEntry extends Component {
  render() {
    return (
      <li className="Result-entry">
        <h3><a
          href={this.props.data.url}
          target="_blank"
        >
          {this.props.data.title}
        </a><span className="score"> {this.props.data.score}</span></h3>
        <p className="url">{this.props.data.url}</p>
        <p>{this.props.data.description}</p>
      </li>
    )
  }
}

export default ResultEntry;
