/* Script containing wrappers used to make elasticsearch queries
All functions will take in a query and a callback
callback will have the arguments (err, resp) where resp will typically be a list
*/

import elasticsearch from 'elasticsearch';

// Elastic client
const client = new elasticsearch.Client({
  host: 'localhost:9200'
});


function _fuzzy_query(field, query) {
  const body = { match: {} }
  body.match[field] = query
  return body
}

function _simple_query(field, query) {
  const body = { fuzzy: {} }
  body.fuzzy[field] = query
  return body
}


function makeSuggestionsA(query, callback, cutoff=0.) {
  /* Make autocomplete suggestions based on query
  (Order unaccounted)
  Args:
    query (string): The string to suggest auto complete options for
    callback (function): A callback that takes in (err, resp) where resp will
      be a list of strings representing suggested auto complete options
  */

  const body = {
    from: 0, size: 5,
    query: {
      match: {
        'phrase.edgengram': query,
      }
    }
  };

  const options = {
    index: 'suggestion_phrases',
    body: body,
  };

  client.search(options, (err, resp, body) => {
    if (err) callback(err, []);

    const max_score = Math.max(resp.hits.max_score, 8);
    const min_score = max_score * cutoff;

    const suggestions = resp.hits.hits.filter(hit => (
      hit._score > min_score
    )).map(hit => {
      // return hit._source.title

      // Visualize score
      const rel_score = hit._score / max_score
      return (hit._source.phrase + ' - ' + rel_score.toString())
    })

    callback(err, suggestions);
  });
}


function makeSuggestionsB(query, callback, cutoff=0.8) {
  /* Make autocomplete suggestions based on query
  (Order accounted)
  Args:
    query (string): The string to suggest auto complete options for
    callback (function): A callback that takes in (err, resp) where resp will
      be a list of strings representing suggested auto complete options
  */

  const body = {
    from: 0, size: 5,
    'suggest': { 'suggest': {
      'prefix': query,
      'completion': {
        'field': 'phrase.completion',
        'fuzzy': { 'fuzziness': Math.ceil(query.length / 10) }
      }
    }}
  };

  const options = {
    index: 'suggestion_phrases',
    body: body,
  };

  client.search(options, (err, resp, body) => {
    if (err) callback(err, []);

    const suggestions = resp.suggest.suggest[0].options.map(
      option => option.text
    );

    callback(err, suggestions);
  });
}


// function makeSuggestionsC(query, callback, cutoff=0.8) {
//   /* Make autocomplete suggestions based on query
//   Aggregated ordered and unordered
//   Args:
//     query (string): The string to suggest auto complete options for
//     callback (function): A callback that takes in (err, resp) where resp will
//       be a list of strings representing suggested auto complete options
//   */
//
//   const body = {
//     from: 0, size: 5,
//     'suggest': { 'suggest': {
//       'prefix': query,
//       'completion': {
//         'field': 'title.completion',
//         'fuzzy': { 'fuzziness': Math.ceil(query.length / 10) }
//       }
//     }}
//   };
//
//   const options = {
//     index: 'suggestion',
//     body: body,
//   };
//
//   client.search(options, (err, resp, body) => {
//     if (err) callback(err, []);
//
//     const suggestions = resp.suggest.suggest[0].options.map(
//       option => option.text
//     );
//
//     callback(err, suggestions);
//   });
// }


function searchMovies(query, from, size, callback) {
  /* Search for a movie in the database based on the query
  Args:
    query (string): The string to suggest auto complete options for
    callback (function): TODO
  */
  const body = {
    from: from, size: size,
    query: {
      bool: {
        should: [
          _fuzzy_query ('title'      , query),
          _simple_query('description', query),
          _simple_query('actors'     , query),
          _simple_query('movies'     , query),
        ]
      }
    }
  };

  const options = {
    index: 'movies',
    body: body,
  };

  return client.search(options, (err, resp, body) => {
    if (err) return;

    const results = resp.hits.hits.map(hit => {
      return {
        title: hit._source.title,
        url: 'https://en.wikipedia.org/wiki/' + hit._source.title.replace(' ', '_'),
        score: hit._score,
        description: hit._source.description
      };
    })

    callback(err, {
      time_taken: resp.took / 1000,
      num_results: resp.hits.total,
      results: results
    });

  })
}


export {
  makeSuggestionsA,
  makeSuggestionsB,
  searchMovies
};
