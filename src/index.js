import React from 'react';
import ReactDOM from 'react-dom';

// Styles and scripts
import './css/index.css';

// App
import App from './App';
import registerServiceWorker from './js/registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
