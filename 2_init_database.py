import os

import collections
import tqdm
import six

import json
import requests

import sqlite3
from sqlite3 import Error

import _2a_init_mapping
import _2b_upload_data


def main():
    # Delete existing database and create mapping
    _2a_init_mapping.main()

    # Add entries into database
    _2b_upload_data.main()


if __name__ == '__main__':
    main()
